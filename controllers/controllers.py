# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import Response
import json
from datetime import date
from datetime import datetime
from datetime import *
import datetime



class IngresosController(http.Controller):

    @http.route('/api/ingresos', auth='public', method=['GET'], csrf=False)
    def get_visits(self, **kw):
        try:
            ingresos = http.request.env['test_model_ingresos'].sudo().search_read([], ['employee_id','tipo_ingre_id','monto_lps', str('fecha_precio')])
            res = json.dumps(ingresos, ensure_ascii=False).encode('utf-8')
            return Response(res, content_type='application/json;charset=utf-8', status=200)
        except Exception as e:
            return Response(json.dumps({'error': str(e)}), content_type='application/json;charset=utf-8', status=505)

  
    @http.route('/api/inse', auth='public', methods=['POST'], csrf=False)
    def insert_ingres(self, **kw):
        try:
            attendance = json.loads(str(http.request.httprequest.data, 'utf-8'))
            http.request.env['test_model_ingresos'].sudo().create(attendance)
            return self.build_response({'message': 'inserted'})
        except Exception as e:
            return self.build_response({'err': str(e)})
            
    def build_response(self, entity):
        response = json.dumps(entity, ensure_ascii=False).encode('utf8')
        return Response(response, content_type='application/json;charset=utf-8', status=200)
